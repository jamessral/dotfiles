
local wezterm = require 'wezterm'

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

config.font = wezterm.font("Noto Sans Mono")
-- no ligatures
config.harfbuzz_features = { 'calt=0', 'clig=0', 'liga=0' }
if wezterm.target_triple == "aarch64-apple-darwin" then
    config.font_size = 14
else
    config.font_size = 11
end

-- function get_appearance()
--   if wezterm.gui then
--     return wezterm.gui.get_appearance()
--   end
--   return 'Dark'
-- end

-- function scheme_for_appearance(appearance)
--   if appearance:find 'Dark' then
--     return 'Mariana'
--   else
--     return 'One Light (Gogh)'
--   end
-- end

-- function opacity_for_appearance(appearance)
--   if appearance:find 'Dark' then
--     return 0.9
--   else
--     return 0.9
--   end
-- end

-- config.color_scheme = scheme_for_appearance(get_appearance())
-- config.window_background_opacity = opacity_for_appearance(get_appearance())

-- For example, changing the color scheme:

config.enable_tab_bar = true
config.line_height = 1.3
config.hide_tab_bar_if_only_one_tab = true

-- and finally, return the configuration to wezterm
return config
