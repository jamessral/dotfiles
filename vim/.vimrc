set nomodeline
set noswapfile
set encoding=UTF-8
set ttyfast
set notermguicolors
set wildmenu
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/.tmp/*,*/.sass-cache/*,*/node_modules/*,*.keep,*.DS_Store,*/.git/*,*/__pychache__/*,*/zig-cache/*,*/.idea/*,*/.vscode/*
set mouse=a
" # http://superuser.com/questions/549930/cant-resize-vim-splits-inside-tmux
if &term =~ '^screen'
  " tmux knows the extended mouse mode
  set ttymouse=xterm2
endif

set path+=**

" Whitespace
set nowrap                      " don't wrap lines
set tabstop=2 shiftwidth=2      " a tab is two spaces (or set this to 2)
set expandtab                   " use spaces, not tabs (optional)
set backspace=indent,eol,start  " backspace through everything in insert mode

set splitright
set splitbelow

" Use the OS clipboard by default
set clipboard^=unnamed
set nocursorline
set showbreak=↪\

" Statusline
set laststatus=2

" Searching
set hlsearch                    " highlight matches
set incsearch                   " incremental searching

" set backup       " keep a backup file
set backupcopy=yes
set undofile
set undodir=~/.vim/undo//

set history=50    " keep 50 lines of command line history
set title " terminal title
set autoread " load change files

let mapleader=' '

" Clears the search register
nmap <silent> <leader>cs :nohlsearch<CR>
" Edit the vimrc file
nnoremap <silent> <leader>ev :e $MYVIMRC<CR>
nnoremap <silent> <leader>rv :so $MYVIMRC<CR>

set shortmess+=c
set omnifunc=syntaxcomplete#Complete

syntax enable
filetype plugin indent on    " required
filetype plugin on


" Fzf
set rtp+=~/.fzf
nnoremap <silent><leader>f :FZF<cr>

" builtin plugins
packadd! matchit
packadd! editorconfig
packadd! comment

" Line Numbers
set number
set relativenumber
function! ShowLinesFunc()
  set number
  set relativenumber
endfunction
function! HideLinesFunc()
  set nonumber
  set norelativenumber
endfunction
command! -nargs=0 ShowLines :call ShowLinesFunc()
command! -nargs=0 HideLines :call HideLinesFunc()

function! RailsTestFunc(path)
  let &makeprg='bin/rails t ' .. a:path
  make
endfunction
command! -nargs=1 RailsTest :call RailsTestFunc(<f-args>)

function! GoRunFunc(path)
  let &makeprg='go run ' .. a:path
  make
endfunction
command! -nargs=1 GoRun :call GoRunFunc(<f-args>)

function! GoFmtFunc()
  execute '!go fmt ' .. expand("%") | edit
endfunction
command! -nargs=0 GoFmt :call GoFmtFunc()
autocmd! BufWritePost *.go :GoFmt

function! PrettierFormatFunc()
  execute '!npx prettier -w ' .. expand("%") | edit
  make
endfunction
command! -nargs=0 PrettierFormat :call PrettierFormatFunc()

function! TSCompFunc()
  let &makeprg='npx tsc -p tsconfig.json'
  make
endfunction
command! -nargs=0 TSComp :call TSCompFunc()


" Syntax Highlighting and File Types
autocmd! FileType swift setlocal tabstop=2 shiftwidth=2 commentstring=//\ %s
autocmd! FileType c setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! FileType cpp setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! FileType go setlocal tabstop=4 shiftwidth=4 noexpandtab
autocmd! FileType odin setlocal tabstop=4 shiftwidth=4 noexpandtab

" Remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

" Spell Checking
autocmd Filetype gitcommit setlocal spell
autocmd Filetype markdown setlocal spell

" Folding
set foldmethod=expr

" GUI Settings
set guifont=Hack:h15
set guioptions-=l
set guioptions-=r
set guioptions-=T
set guioptions-=R
set guioptions-=m
set guioptions-=L

" Use Ag
set grepprg=ag\ --vimgrep

function! LoadDarkFunc()
  set background=dark

  " if has("gui_running")
    colo wildcharm
  " endif

 endfunction
command! -nargs=0 LoadDark :call LoadDarkFunc()

function! LoadLightFunc()
  set background=light

  " if has("gui_running")
    colo wildcharm
  " endif

endfunction
command! -nargs=0 LoadLight :call LoadLightFunc()
"
LoadDark
