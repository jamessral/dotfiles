;;; Gotta go fast
(when (boundp 'w32-pipe-read-delay)
  (setq w32-pipe-read-delay 0))

(setq-default no-native-compile t)
(setq comp-deferred-compilation nil)

(menu-bar-mode -1)
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(require 'cl)

(setf gc-cons-threshold 100000000)
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

;; Package initialize called in early-init
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (setq package-archives '(("melpa" . "https://melpa.org/packages/")
					       ("gnu" . "https://elpa.gnu.org/packages/"))))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-defer t)
(setq use-package-always-ensure t)

;; join line to next line
(defun concat-lines ()
  (interactive)
  (join-line -1))

;; comments
(defun toggle-comment-on-line ()
  "comment or uncomment current line"
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))

;;; Basics
;; Borrowed from https://sam217pa.github.io/2016/09/02/how-to-build-your-own-spacemacs/
(setq delete-old-versions -1)
(setq version-control t)
(setq vc-make-backup-files t)
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")) ) ; which directory to put backups file
(setq vc-follow-symlinks t )				       ; don't ask for confirmation when opening symlinked file
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)) ) ;transform backups file name
(setq inhibit-startup-screen t )	; inhibit useless and old-school startup screen
(setq ring-bell-function 'ignore )	; silent bell when you make a mistake
(set-charset-priority 'unicode)
(setq locale-coding-system 'utf-8 )	; use utf-8 by default
(setq coding-system-for-read 'utf-8 )	; use utf-8 by default
(setq coding-system-for-write 'utf-8 )
(prefer-coding-system 'utf-8)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))
(setq default-fill-column 80)
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
;; Mac key admustments
(setq mac-option-modifier 'control)
(setq mac-command-modifier 'meta)

;; UI
;; Turn off the menu bar at the top of each frame because it's distracting
(global-hl-line-mode -1)
(global-display-line-numbers-mode -1)

(setq system-uses-terminfo nil)
(prefer-coding-system 'utf-8)

(add-to-list 'comint-output-filter-functions 'ansi-color-process-output)
(ansi-color-for-comint-mode-on)

(display-time-mode 1)

(setq linum-format " %d ")

(setq linum-relative-current-symbol "")

(global-set-key (kbd "C-c C-c") 'recompile)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "C-h") 'help)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "M-\\") 'split-window-right)
(global-set-key (kbd "M-_") 'split-window-below)
(global-set-key (kbd "C-j") 'concat-lines)
(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "C-t") 'transpose-chars)
(global-set-key (kbd "C-]") 'er/expand-region)
(global-set-key (kbd "C-.") 'company-files)
(global-set-key (kbd "C-'") 'company-complete)
(global-set-key (kbd "C-;") 'toggle-comment-on-line)
;; don't pop up font menu
(global-set-key (kbd "s-t") '(lambda () (interactive)))

(setq
 x-select-enable-clipboard t
 x-select-enable-primary t
 save-interprogram-paste-before-kill t
 apropos-do-all t
 mouse-yank-at-point t)

;; full path in title bar ()
(setq-default frame-title-format "%b (%f)")

;; no bell
(setq ring-bell-function 'ignore)

;; Font
(set-face-attribute 'default nil :font "Hack")
(if (memq window-system '(ns))
	(set-face-attribute 'default nil :height 150)
  (set-face-attribute 'default nil :height 110))


;; Save history of minibuffer
(setq history-length 25)
(savehist-mode)

;; For help, see: https://www.masteringemacs.org/article/understanding-minibuffer-completion
(setopt enable-recursive-minibuffers t)                ; Use the minibuffer whilst in the minibuffer
(setopt completion-cycle-threshold 1)                  ; TAB cycles candidates
(setopt completions-detailed t)                        ; Show annotations
(setopt tab-always-indent 'complete)                   ; When I hit TAB, try to complete, otherwise, indent
(setopt completion-styles '(basic initials substring)) ; Different styles to match input to candidates

(setopt completion-auto-help 'always)                  ; Open completion always; `lazy' another option
(setopt completions-max-height 20)                     ; This is arbitrary
(setopt completions-detailed t)
(setopt completions-format 'one-column)
(setopt completions-group t)
(setopt completion-auto-select 'second-tab)            ; Much more eage
(keymap-set minibuffer-mode-map "TAB" 'minibuffer-complete) ; TAB acts more like how it does in the shell


;; Add some margins
(setq-default left-margin-width 2 right-margin-width 2)
(setq-default header-line-format "")

;; Revert buffers when underlying file has changed
(global-auto-revert-mode t)
;;; End Basics

(use-package ag
  :ensure t)

(add-to-list 'load-path "./vendor")

;; Don't show native OS scroll bars for buffers because they're redundant
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))

(defadvice load-theme
    ;; Make sure to disable current colors before switching
    (before theme-dont-propagate activate)
  (mapc #'disable-theme custom-enabled-themes))

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(add-to-list 'load-path "~/.emacs.d/themes")

;; Font
(defun jas/load-font (font-name)
  "Helper to make it easier to switch fonts"
  (interactive)
  (set-face-attribute 'default nil :font font-name))

(defun jas/initialize-fonts ()
  "Fonts setup"
  (interactive)
  (jas/load-font "Hack")
  (if (memq window-system '(ns))
	(set-face-attribute 'default nil :height 150)
	(set-face-attribute 'default nil :height 110)))

(add-hook 'after-init-hook #'jas/initialize-fonts)

(defun load-dark ()
  "Load Dark Color Scheme."
  (interactive)
  (load-theme 'wombat t))

(defun load-light ()
  "Load Light Color Scheme."
  (interactive)
  (load-theme 'adwaita t))

(setq-default default-frame-background-mode 'dark)
(setq default-frame-background-mode 'dark)

(load-dark)

(add-to-list 'default-frame-alist '(undecorated-round . t))

(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files
(setq create-lockfiles nil)

;; Show tabs as 4 spaces
(setq tab-width 4)

;; Use subword mode
(global-subword-mode)

(defun colorize-compilation-buffer ()
  (toggle-read-only)
  (ansi-color-apply-on-region compilation-filter-start (point))
  (toggle-read-only))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

(add-hook 'before-save-hook #'delete-trailing-whitespace)

;; fix weird os x kill error
(defun ns-get-pasteboard ()
  "Returns the value of the pasteboard, or nil for unsupported formats."
  (condition-case nil
                  (ns-get-selection-internal 'CLIPBOARD)
                  (quit nil)))

(setq electric-indent-mode 1)
(setq-default indent-tabs-mode nil)
(setq indent-tabs-mode nil)

;;; Some Bascis
(use-package exec-path-from-shell
  :commands (exec-path-from-shell-initialize exec-path-from-shell-copy-env)
  :ensure t
  :init
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize)
    (exec-path-from-shell-copy-envs
     '("PATH" "ANDROID_HOME"))))

;;; End Basics

;;; OrgMode
(use-package org
  :ensure t
  :config
  (setq org-agenda-files (list "~/org/work.org"
							   "~/org/personal.org"))
  (setq org-capture-templates
      '(("W" "Work todo" entry (file+headline "~/org/work.org" "Tasks")
         "* TODO %?\n  %i\n  %a")
		("P" "Personal todo" entry (file+headline "~/org/personal.org" "Tasks")
         "* TODO %?\n  %i\n  %a")
        ("w" "Work Notes" entry (file+datetree "~/org/work.org")
         "* %?\nEntered on %U\n  %i\n  %a")
		("p" "Personal Notes" entry (file+datetree "~/org/personal.org")
         "* %?\nEntered on %U\n  %i\n  %a"))))

(use-package org-bullets
  :ensure t)
(add-hook 'org-mode-hook (lambda ()
                           (org-bullets-mode 1)))

(defun jas/go-to-file-in-split (filepath)
  "Open file in split.  FILEPATH is an absolute path to file."
  (interactive)
  (split-window-below)
  (windmove-down)
  (find-file filepath))

(defun jas/go-to-work-org-file ()
  "Edit my work org file."
  (interactive)
  (jas/go-to-file-in-split "~/org/work.org"))

(global-set-key (kbd "C-c o w") 'jas/go-to-work-org-file)

(defun jas/go-to-personal-org-file ()
  "Edit my personal org file."
  (interactive)
  (jas/go-to-file-in-split "~/org/personal.org"))

(global-set-key (kbd "C-c o p") 'jas/go-to-work-org-file)

;;; End OrgMode

;;; Ruby
(use-package ruby-test-mode
  :ensure t)
;;; End Ruby

;;; Git
(use-package magit
  :ensure t)
;;; End Git

;;; Javascript
(setq js-indent-level 2)
(setq js-jsx-indent-level 2)
(add-hook 'js-mode-hook
          (function
           (lambda ()
             (setq js-indent-level 2
                   js-jsx-indent-level 2
                   indent-tabs-mode nil))))
;;; End Javascript

;; Coffeescript
(use-package coffee-mode
  :ensure t)
;;; End Coffeescript

;;; Lua
(use-package lua-mode
  :ensure t)
;;; End Lua

;;; Swift
(use-package swift-mode
  :ensure t
  :config
  (setq-default swift-mode:basic-offset 2))

;;; End Swift

;;; SCSS
(use-package sass-mode
  :ensure t)
;;; END SCSS

(use-package yaml-mode
  :defer t
  :ensure t)

(use-package dumb-jump
  :ensure t
  :init
  (setq dumb-jump-force-searcher 'ag)
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

;;; C/C++
(setq-default c-default-style "linux"
      tab-width 4
      indent-tabs-mode t
      c-basic-offset 4)
(setq-default c-basic-offset 4)

(add-hook 'c-mode-hook
		  (lambda ()
			(local-set-key (kbd "C-c C-c") 'recompile)))
(add-hook 'c++-mode-hook
		  (lambda ()
			(local-set-key (kbd "C-c C-c") 'recompile)))
;; End C/C++
(setq css-indent-offset 2)
;;; End UI
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(ag coffee-mode dumb-jump exec-path-from-shell lua-mode magit
		org-bullets ruby-test-mode sass-mode swift-mode yaml-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
