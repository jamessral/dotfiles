;;; early-init.el --- early bird  -*- no-byte-compile: t -*-
(setenv "PATH" "/opt/homebrew/bin:/opt/homebrew/sbin:/Users/jamessral/.asdf/shims:/Users/jamessral/.asdf/bin:/Users/jamessral/.asdf/installs/ruby/3.3.4/bin:/usr/local/bin:/System/Cryptexes/App/usr/bin:/usr/bin:/bin:/usr/sbin:/sbin:/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/local/bin:/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/bin:/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/appleinternal/bin:/Library/Apple/usr/bin:/Applications/kitty.app/Contents/MacOS:/Users/jamessral/Library/Android/sdk/emulator:/Users/jamessral/Library/Android/sdk/platform-tools:/Users/jamessral/.local/bin:/opt/homebrew/opt/fzf/bin:/Users/jamessral/Library/Application Support/JetBrains/Toolbox/scripts:/Users/jamessral/.orbstack/bin")
(setq exec-path (split-string (getenv "PATH") path-separator))
(setq package-enable-at-startup nil)
(require 'package)
(setq load-prefer-newer t)
(setq-default no-native-compile t)
(setq comp-deferred-compilation nil)
(package-initialize)
;;; early-init.el ends here
