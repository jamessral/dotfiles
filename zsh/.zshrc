export TERM="xterm-256color"
export EDITOR="subl -w"
export VISUAL="subl -w"

bindkey -e
bindkey -s ^o "tmux-sessionizer\n"

#Aliases
alias gs="git status"
alias gc="git commit"
alias ga="git add ."
alias gp="git push"
alias gpf="git push --force-with-lease"
alias gP="git pull"
alias gu="git fetch && git rebase -i origin/master"
alias gf="git fetch"
alias grh2="git rebase HEAD~2 -i"
alias mux="tmuxinator"
alias lg="lazygit"
alias ls="eza"
alias rt="bin/rails t"
alias rs="bin/rails s"
alias rc="bin/rails c"
alias \?="jsearch"

export PATH=$PATH:$HOME/.local/bin

export NOPRIDE=1

# Android
export JAVA_HOME=/Library/Java/JavaVirtualMachines/zulu-17.jdk/Contents/Home
export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/platform-tools

# Golang
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

if [ -f "/etc/arch-release" ]; then
  # Use powerline
  USE_POWERLINE="true"
  # Has weird character width
  # Example:
  #    is not a diamond
  HAS_WIDECHARS="false"
  # Source manjaro-zsh-configuration
  if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
    source /usr/share/zsh/manjaro-zsh-config
  fi
  # Use manjaro zsh prompt
  if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
    source /usr/share/zsh/manjaro-zsh-prompt
  fi

  function updateSublime {
    cd ~/.config/sublime-text/Packages/User
    git add .
    git commit
    git push
    cd -
  }

  function getLatestSublime {
    cd ~/.config/sublime-text/Packages/User
    git pull
    cd -
  }
else
  eval "$(/opt/homebrew/bin/brew shellenv)"
  autoload -U promptinit; promptinit
  prompt pure

  function updateSublime {
    cd ~/Library/Application\ Support/Sublime\ Text/Packages/User
    git add .
    git commit
    git push
    cd -
  }

  function getLatestSublime {
    cd ~/Library/Application\ Support/Sublime\ Text/Packages/User
    git pull
    cd -
  }

  export PATH="${ASDF_DATA_DIR:-$HOME/.asdf}/shims:$PATH"
fi
[ -f ~/.fzf/.fzf.zsh ] && source ~/.fzf/.fzf.zsh
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
