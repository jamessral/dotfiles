if [ -f "/etc/arch-release" ]; then
  # don't run homebrew on linux
else
  eval "$(/opt/homebrew/bin/brew shellenv)"
  export PATH="${ASDF_DATA_DIR:-$HOME/.asdf}/shims:$PATH"
fi


# Android
export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/platform-tools
