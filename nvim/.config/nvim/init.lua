-- Options
vim.opt.path:append("**")
vim.opt.wildmenu = true
vim.opt.wildignore:append(
    "*/tmp/*,*.so,*.swp,*.zip,*/.tmp/*,*/.sass-cache/*,*/node_modules/*,*.keep,*.DS_Store,*/.git/*,*/__pychache__/*"
)
vim.opt.mouse = "a"
vim.g.mapleader = " "
vim.opt.hidden = true

-- GUI Settings
vim.opt.guicursor = ""

-- Whitespace
vim.opt.tabstop = 4
vim.opt.shiftwidth = 2                 -- a tab is four spaces (or vim.opt.this to 4)
vim.opt.expandtab = true               -- use spaces, not tabs (optional)
vim.opt.backspace = "indent,eol,start" -- backspace through everything in insert mode
vim.opt.signcolumn = "yes"
vim.opt.modelines = 0

-- Vertical split to right
vim.opt.splitright = true

-- Horizontal split below
vim.opt.splitbelow = true

-- Use the OS clipboard by default
vim.opt.clipboard = "unnamedplus"

-- Statusline
vim.opt.laststatus = 2

vim.opt.showbreak = "↪\\"

-- Searching
vim.opt.hlsearch = true   -- highlight matches
vim.opt.incsearch = true  -- incremental searching
vim.opt.ignorecase = true -- searches are case insensitive...
vim.opt.smartcase = true  -- ... unless they contain at least one capital letter
vim.opt.backupcopy = "yes"
vim.opt.undofile = true

vim.opt.history = 50 --   keep 50 lines of command line history
vim.opt.title = true
vim.opt.autoread = true
vim.opt.shortmess:append("c")
vim.opt.relativenumber = true
vim.opt.number = true

vim.cmd([[ filetype plugin indent on ]])
vim.cmd([[ filetype on ]])
vim.cmd([[ set omnifunc=syntaxcomplete#Complete ]])

-- UI
CurrentTheme = "dark"
function DarkTheme()
    vim.cmd("colo carbonfox")
    vim.cmd.set("background=dark")
    CurrentTheme = "dark"
end

function LightTheme()
    vim.cmd("colo dayfox")
    vim.cmd.set("background=light")
    CurrentTheme = "light"
end

function SwitchTheme()
    if CurrentTheme == "dark" then
        LightTheme()
    elseif CurrentTheme == "light" then
        DarkTheme()
    end
end

-- Mappings
vim.api.nvim_set_keymap("n", "<leader>f", ":Telescope find_files<cr>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>b", ":Telescope buffers<cr>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>s", ":Telescope live_grep<cr>", { noremap = true, silent = true })

vim.api.nvim_set_keymap("i", "<C-c>", "<Esc>", { noremap = true })
vim.api.nvim_set_keymap("x", "<C-c>", "<Esc>", { noremap = true })

vim.api.nvim_set_keymap("n", "<leader>h", ":sp<cr>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>v", ":vs<cr>", { noremap = true })

vim.api.nvim_set_keymap("n", "<leader>cs", ":nohlsearch<CR>", { silent = true })
vim.api.nvim_set_keymap("n", "<leader>wf", ":w<CR>", { silent = true })
vim.api.nvim_set_keymap("n", "<leader>cc", ":make<cr>", { silent = true })
vim.api.nvim_set_keymap("n", "<leader>gs", ":Git<cr>", { silent = true })

vim.api.nvim_set_keymap("n", "<leader>ev", ":e $MYVIMRC<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>rv", ":so $MYVIMRC<CR>", { noremap = true, silent = true })

vim.api.nvim_set_keymap("n", "<leader>tt", ":TestNearest<cr>", { silent = true })
vim.api.nvim_set_keymap("n", "<leader>tf", ":TestFile<cr>", { silent = true })

-- LSP
local bufopts = { noremap = true, silent = true, buffer = bufnr }
vim.api.nvim_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>', bufopts)
vim.api.nvim_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>', bufopts)
vim.api.nvim_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', bufopts)
vim.api.nvim_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>', bufopts)
vim.api.nvim_set_keymap('n', '<leader>K', '<cmd>lua vim.lsp.buf.signature_help()<cr>', bufopts)
vim.api.nvim_set_keymap('n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<cr>', bufopts)
vim.api.nvim_set_keymap('n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<cr>', bufopts)
vim.api.nvim_set_keymap('n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<cr>', bufopts)
vim.api.nvim_set_keymap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<cr>', bufopts)
vim.api.nvim_set_keymap('n', '<leader>ac', '<cmd>lua vim.lsp.buf.code_action()<cr>', bufopts)
vim.api.nvim_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<cr>', bufopts)
vim.api.nvim_set_keymap('n', '<leader>F', '<cmd>lua vim.lsp.buf.format()<cr>', bufopts)


vim.api.nvim_set_keymap('n', '<leader>S', '<cmd>lua require("spectre").toggle()<CR>', { silent = true })

vim.api.nvim_set_keymap('n', '<F5>', '<cmd>lua SwitchTheme()<cr>', { noremap = true, silent = true, buffer = bufnr })
-- Use Ag
vim.opt.grepprg = "ag --vimgrep"

-- Filetype defaults
vim.cmd([[
autocmd! FileType lua setlocal tabstop=4 shiftwidth=4
autocmd! FileType c setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! FileType cpp setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! FileType go setlocal tabstop=4 shiftwidth=4 noexpandtab commentstring=//\ %s
autocmd! FileType erlang setlocal tabstop=4 shiftwidth=4
autocmd! BufNewFile,BufRead *.cpp setlocal tabstop=4 shiftwidth=4
autocmd! BufNewFile,BufRead *.go setlocal tabstop=4 shiftwidth=4
autocmd! BufNewFile,BufRead *.erl setlocal tabstop=4 shiftwidth=4
autocmd! BufNewFile,BufRead *.c setlocal tabstop=4 shiftwidth=4
autocmd! BufNewFile,BufRead *.js setlocal tabstop=2 shiftwidth=2
autocmd! BufWritePre * %s/\s\+$//e
]])

-- Packages
-- Bootstrap Lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)
require("lazy").setup({
    {
        "nvim-telescope/telescope.nvim",
        branch = "0.1.x",
        dependencies = { "nvim-lua/plenary.nvim" },
        config = function()
            require("telescope").setup({
                defaults = { color_devicons = false },
            })
        end,
    },
    {
        "nvim-pack/nvim-spectre",
        dependencies = { "nvim-lua/plenary.nvim" },
    },
    {
        "neovim/nvim-lspconfig",
        config = function()
            local lspconfig = require('lspconfig')
            -- lspconfig.ts_ls.setup {}
            lspconfig.lua_ls.setup {}
            lspconfig.ruby_lsp.setup {
                init_options = {
                    formatter = nil,
                    linters = {},
                },
            }
            lspconfig.stimulus_ls.setup {}
            lspconfig.ts_ls.setup {}
            lspconfig.sourcekit.setup {
                capabilities = {
                    workspace = {
                        didChangeWatchedFiles = {
                            dynamicRegistration = true,
                        },
                    },
                },
                root_dir = lspconfig.util.root_pattern(
                    '.git',
                    'Package.swift',
                    'compile_commands.json'
                ),
            }
        end
    },
    { "rafamadriz/friendly-snippets" },
    {
        "williamboman/mason.nvim",
        dependencies = {
            "williamboman/mason-lspconfig.nvim",
            "neovim/nvim-lspconfig",
        },
        config = function()
            require("mason").setup()
            require("mason-lspconfig").setup({
                ensure_installed = {
                    "ruby_lsp",
                    "stimulus_ls",
                    "ts_ls",
                    "lua_ls",
                    "gopls",
                    "rust_analyzer",
                },
            })
        end
    },
    {
        "hrsh7th/nvim-cmp",
        dependencies = {
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-nvim-lsp",
            "saadparwaiz1/cmp_luasnip",
        },
        config = function()
            local cmp = require('cmp')
            cmp.setup({
                completion = {
                    -- autocomplete = false,
                },
                sources = cmp.config.sources({
                    { name = 'nvim_lsp' },
                    { name = 'path' },
                    {
                        name = 'buffer',
                        option = {
                            get_bufnrs = function()
                                return vim.api.nvim_list_bufs()
                            end
                        },
                        keyword_length = 2,
                    },
                }),
                mapping = cmp.mapping.preset.insert({
                    ['<C-e>'] = cmp.mapping.abort(),
                    ['<C-y>'] = cmp.mapping.confirm({ select = true }),
                    ['<C-Space>'] = cmp.mapping.complete(),
                }),
            })
        end,
    },
    { "EdenEast/nightfox.nvim" },
    {
        "L3MON4D3/LuaSnip",
        version = "v2.*",
        build = "make install_jsregexp",
        config = function()
            require("luasnip.loaders.from_vscode").lazy_load()
        end
    },
    {
        "folke/tokyonight.nvim",
        lazy = false,
        priority = 1000,
        opts = {},
    },
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' },
        config = function()
            require('lualine').setup()
        end,
    },
    {
        "stevearc/conform.nvim",
        config = function()
            require("conform").setup({
                formatters_by_ft = {
                    lua = { "stylua" },
                    go = { "goimports", "gofmt" },
                    javascript = { "prettier" },
                },
                format_on_save = {
                    timeout_ms = 500,
                },
            })
        end,
    },
    {
        'windwp/nvim-autopairs',
        event = "InsertEnter",
        config = true
    },
    {
        "lervag/vimtex",
        lazy = false,
    },
    "tpope/vim-fugitive",
    {
        "lewis6991/gitsigns.nvim",
        config = function()
            require("gitsigns").setup {}
        end
    },
    {
        "christoomey/vim-tmux-navigator",
        cmd = {
            "TmuxNavigateLeft",
            "TmuxNavigateDown",
            "TmuxNavigateUp",
            "TmuxNavigateRight",
            "TmuxNavigatePrevious",
            "TmuxNavigatorProcessList",
        },
        keys = {
            { "<c-h>",  "<cmd><C-U>TmuxNavigateLeft<cr>" },
            { "<c-j>",  "<cmd><C-U>TmuxNavigateDown<cr>" },
            { "<c-k>",  "<cmd><C-U>TmuxNavigateUp<cr>" },
            { "<c-l>",  "<cmd><C-U>TmuxNavigateRight<cr>" },
            { "<c-\\>", "<cmd><C-U>TmuxNavigatePrevious<cr>" },
        },
    },
    {
        "vim-test/vim-test",
        config = function()
            vim.g["test#strategy"] = "neovim"
        end
    }
})

LightTheme()
