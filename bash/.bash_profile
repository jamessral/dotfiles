# .bash_profile
# Get the aliases and functions
#
if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi

# User specific environment and startup programs
export EDITOR="subl -w"
export VISUAL="subl -w"

bind -m emacs
bind -x '"\C-u":"tmux-sessionizer\n"'

alias gs="git status"
alias ga="git add ."
alias gp="git push"
alias gpf="git push --force-with-lease"
alias gP="git pull"
alias gc="git commit"
alias gf="git fetch"
alias gu="git fetch && git rebase origin/master"
alias grh2="git rebase -i HEAD~2"
alias mux="tmuxinator"
alias lg="lazygit"
alias ls="eza"

export PATH=$PATH:$HOME/.local/bin

# Android
export JAVA_HOME=/Library/Java/JavaVirtualMachines/zulu-17.jdk/Contents/Home
export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/platform-tools

# Golang
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

. "$HOME/.asdf/asdf.sh"
. "$HOME/.asdf/completions/asdf.bash"

if command -v apt > /dev/null; then
  setxkbmap -option "ctrl:nocaps"
else
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
